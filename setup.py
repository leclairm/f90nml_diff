import os
from setuptools import setup

def get_version():
    with open('f90nml_diff/__init__.py') as f:
        for line in f:
            if line.startswith('__version__'):
                _, _, version = line.replace("'", '').split()
                break
    return version

setup(name='f90nml_diff',
      version=get_version(),
      description="Compare 2 f90 namelist files",
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      url="https://git.iac.ethz.ch/leclairm/f90nml_diff",
      packages=['f90nml_diff'],
      entry_points={'console_scripts': ['nmldiff = f90nml_diff.make_diff:make_diff']},
      install_requires=['f90nml>=1.0.2']
)

import f90nml
from os.path import abspath, relpath, commonpath
from argparse import ArgumentParser


def nmldiff():

    parser = ArgumentParser(description="Compare 2 namelist files")
    parser.add_argument('path1')
    parser.add_argument('path2')
    args = parser.parse_args()

    nml_dict1, nml_dict2 = f90nml.read(args.path1), f90nml.read(args.path2)
    fname1, fname2 = _min_path(path1, path2)

    common_nmls, output = _get_common_nmls(nml_dict1, nml_dict2, fname1, fname2)

    for name, (nml1, nml2) in common_nmls.items():
        messages = _diff_nml(nml1, nml2, fname1, fname2)
        if len(messages) > 0:
            if len(output) > 0:
                output += '\n'
            output += '&' + name + '\n' + messages

    if len(output) > 0:
        print(output.rstrip())


def _min_path(path1, path2):

    p1, p2 = abspath(path1), abspath(path2)
    cp = commonpath((p1, p2))
    return relpath(p1, start=cp), relpath(p2, start=cp)


def _get_common_nmls(nml_dict1, nml_dict2, fname1, fname2):

    nml_only_in_msg = "namelist &{:s} only present in {:s}\n"
    n_nml_diff_msg = "number of &{:s} namelists differs\n    {:d} in {:s}\n    {:d} in {:s}\n"

    common_nmls = {}
    messages = ''

    for name, nml1 in nml_dict1.items():
        if name not in nml_dict2:
            messages += nml_only_in_msg.format(name, fname1)
        else:
            nml2 = nml_dict2[name]
            if isinstance(nml1, list):
                if isinstance(nml2, list):
                    l1, l2 = len(nml1), len(nml2)
                    if l1 != l2:
                        messages += n_nml_diff_msg.format(name, l1, fname1, l2, fname2)
                    for k in range(max(l1, l2)):
                        common_nmls["{:s}_{:d}".format(name, k)] = [nml1[k], nml2[k]]
                else:
                    messages += n_nml_diff_msg.format(name, len(nml1), fname1, 1, fname2)
                    common_nmls["{:s}_0".format(name)] = [nml1[0], nml2]
            elif isinstance(nml2, list):
                messages += n_nml_diff_msg.format(name, 1, fname1, len(nml2), fname2)
                common_nmls["{:s}_0".format(name)] = [nml1, nml2[0]]
            else:
                common_nmls[name] = [nml1, nml2]

    for name, nml2 in nml_dict2.items():
        if name not in nml_dict1:
            messages += nml_only_in_msg.format(name, fname2)

    return common_nmls, messages


def _diff_nml(nml1, nml2, fname1, fname2):

    param_only_in_msg = "parameter '{:s}' = {:s} only present in {:s}\n"
    param_diff_msg = "parameter '{:s}' differs\n    {:s} : {:s}\n    {:s} : {:s}\n"

    common_params = {}
    messages = ''

    for param, val1 in nml1.items():
        if param not in nml2:
            messages += param_only_in_msg.format(param, str(val1), fname1)
        else:
            common_params[param] = [val1, nml2[param]]

    for param, val2 in nml2.items():
        if param not in nml1:
            messages += param_only_in_msg.format(param, str(val2), fname2)

    for param, (val1, val2) in common_params.items():
        if val2 != val1:
            messages += param_diff_msg.format(param, fname1, str(val1), fname2, str(val2))

    return messages

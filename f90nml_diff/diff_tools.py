import f90nml


def build_nml_diff(nml1, nml2):

    nml_diff = {}

    for g_name, grp1 in nml1.items():
        if g_name not in nml2:
            grp2 = [None] if isinstance(grp1, list) else None
        else:
            grp2 = nml2[g_name]
        if isinstance(grp1, list) or isinstance(grp2, list):
            if not isinstance(grp1, list):
                grp1 = [grp1]
            if not isinstance(grp2, list):
                grp1 = [grp2]
            l1, l2 = len(grp1), len(grp2)
            l = max(l1, l2)
            grp1 += [None] * (l-l1)
            grp2 += [None] * (l-l2)
            for k in range(l):
                nml_diff['{:s}_{:d}'.format(g_name, k)] = build_grp_diff(grp1[k], grp2[k])
        else:
            nml_diff[g_name] = build_grp_diff(grp1, grp2)

    for g_name, grp2 in nml2.items():
        if g_name not in nml1:
            if isinstance(grp2, list):
                for k in range(len(grp2)):
                    nml_diff['{:s}_{:d}'.format(g_name, k)] = build_grp_diff(None, grp2[k])
            else:
                nml_diff[g_name] = build_grp_diff(None, grp2)

    return nml_diff


def build_grp_diff(grp1, grp2):

    if grp1 is None:
        return {p_name: [None, p_val2] for p_name, p_val2 in grp2.items()}
    elif grp2 is None:
        return {p_name: [p_val1, None] for p_name, p_val1 in grp1.items()}
    else:
        grp_diff = {}
        for p_name, p_val1 in grp1.items():
            if p_name not in grp2:
                grp_diff[p_name] = [p_val1, None]
            else:
                p_val2 = grp2[p_name]
                if p_val1 != p_val2:
                    grp_diff[p_name] = [p_val1, p_val2]
        for p_name, p_val2 in grp2.items():
            if p_name not in grp1:
                grp_diff[p_name] = [None, p_val2]
        return grp_diff


def nml_diff_to_str(nml_diff, nml1, nml2, width=30):

    nml_strs = []
    for grp_name, grp_diff in nml_diff.items():
        if grp_diff:
            nml_strs += [grp_diff_to_str(grp_name, grp_diff, nml1, nml2, width=width)]

    if not nml_strs:
        return 'Namelist files are identical'

    return '\n\n'.join(nml_strs)


def grp_diff_to_str(grp_name, grp_diff, nml1, nml2, width=30):

    if not grp_diff:
        return ''

    str_list = ['&' + grp_name]
    width_p = 0
    nml1.column_width = width
    nml2.column_width = width
    for p_name, (p1, p2) in sorted(grp_diff.items()):
        width_p = max(width_p, len(p_name))
        if p1 is None:
            p1_strs = ['-']
        else:
            p1_strs = [l.lstrip(' =') for l in nml1._var_strings('', p1)]
        if p2 is None:
            p2_strs = ['-']
        else:
            p2_strs = [l.lstrip(' =') for l in nml1._var_strings('', p2)]
        l1, l2 = len(p1_strs), len(p2_strs)
        l = max(l1, l2)
        p1_strs += [''] * (l-l1)
        p2_strs += [''] * (l-l2)
        str_list += [[p_name, p1_strs[0], p2_strs[0]]]
        for k in range(1, l):
            str_list += [['', p1_strs[k], p2_strs[k]]]

    just_str_list = [str_list[0]]
    for line in str_list[1:]:
        p_name = line[0].ljust(width_p)
        p1_str = line[1][:width-3] + '...' if len(line[1]) > width else line[1].ljust(width)
        p2_str = line[2][:width-3] + '...' if len(line[2]) > width else line[2].ljust(width)
        just_str_list += ['  '.join([p_name, p1_str, p2_str])]

    return '\n'.join(just_str_list)

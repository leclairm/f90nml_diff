from argparse import ArgumentParser
import f90nml
from .diff_tools import build_nml_diff, nml_diff_to_str


def make_diff():

    parser = ArgumentParser(description="Compare 2 namelist files")
    parser.add_argument('path1')
    parser.add_argument('path2')
    parser.add_argument('--width', help='width of value columns in characters (default: 30)',
                        type=int, default=30)
    args = parser.parse_args()

    nml1, nml2 = f90nml.read(args.path1), f90nml.read(args.path2)
    nml_diff = build_nml_diff(nml1, nml2)
    print(nml_diff_to_str(nml_diff, nml1, nml2, width=args.width))
